package pl.akademiakodu.peoplegroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeoplegroupApplication {

    public static void main(String[] args) {
        SpringApplication.run(PeoplegroupApplication.class, args);
    }
}
