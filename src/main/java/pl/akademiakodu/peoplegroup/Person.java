package pl.akademiakodu.peoplegroup;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Person {
    private String name;
    private String surname;
    private String sex;

    private static List<Person> peopleFromForm = new ArrayList<>();


    public void addToList(){
        peopleFromForm.add(this);
    }

    static public List<Person> returnPeopleList (){
        return peopleFromForm;
    }
}
