package pl.akademiakodu.peoplegroup;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class AddController {

    @GetMapping("/add")
    public String addPerson(@ModelAttribute Person person, ModelMap map){
        map.put("person", person);
        person.addToList();
        map.put("people", Person.returnPeopleList());
        return "add";
    }
}
